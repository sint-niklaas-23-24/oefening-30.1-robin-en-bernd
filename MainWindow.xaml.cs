﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Oefening_30._1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        Bankrekening bankrekening;
        Figuur figuur;

        private void btnIntrest_Click(object sender, RoutedEventArgs e)
        {
            Bereken(bankrekening);
        }

        private void btnOppervlakte_Click(object sender, RoutedEventArgs e)
        {
            Bereken(figuur);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            bankrekening = new Bankrekening(2.3, 1000);
            figuur = new Figuur(30, 10);
        }
        private void Bereken(IInterfaceObject interfaceObject)
        {
            MessageBox.Show(interfaceObject.ToString());
        }
    }
}
