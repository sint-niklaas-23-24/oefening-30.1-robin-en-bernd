﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_30._1
{
    internal class Bankrekening : IInterfaceObject
    {
        private double _rentevoet;
        private double _saldo;

        public Bankrekening(double rentevoet, double saldo)
        {
            Rentevoet = rentevoet;
            Saldo = saldo;
        }
        public double Rentevoet 
        { 
            get { return _rentevoet;}
            set { _rentevoet = value;}
        }
        public double Saldo 
        { 
            get { return _saldo;} 
            set {  _saldo = value;} 
        }
        public double Bereken()
        {
            return Saldo * Rentevoet/100;
        }
        public override string ToString()
        {
            return $"De intrest van de rekening met saldo \u20AC {Saldo.ToString("0.00")} en rentevoet {Rentevoet.ToString("0.00")}% is {Environment.NewLine} \u20AC {Bereken().ToString("0.00")}.";
        }
    }
}
