﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_30._1
{
    internal class Figuur : IInterfaceObject
    {
        private double _lengte;
        private double _breedte;

        public Figuur(double lengte, double breedte)
        {
            Lengte = lengte;
            Breedte = breedte;
        }
        public double Lengte 
        { 
            get { return _lengte; } 
            set { _lengte = value; } 
        } 
        public double Breedte 
        { 
            get { return _breedte; }
            set { _breedte = value; } 
        }
        public double Bereken()
        {
            return Lengte * Breedte;
        }
        public override string ToString()
        {
            return $"De oppervlakte van de figuur met lengte {Lengte.ToString("0.00")} cm en breedte {Breedte.ToString("0.00")} cm is {Bereken().ToString("0.00")} cm².";
        }
    }
}
